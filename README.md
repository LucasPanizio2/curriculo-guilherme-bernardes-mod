# Guilherme Bernardes de Carvalho Filho

---

# Dados Pessoais:

- CPF: 402.214.534.90
- Dada de Nascimento:
- EMAIL: guilherme.unimar@gmail.com
- TEL: (14) 99088-4856
- Estado Civíl: 
- Endereço: 

---

# Formação Acadêmica:

- Ensino Médio: Colégio Criativo - 2014 até 2016
- Ensino Superior: Unimar - Análise e Desenvolvimento de Sistemas - 2023 até 2025

---

# Experiência:

- Loja do Puxador Marimetal
- 2018 até ATUALMENTE
- Técnico de E-Commerce

---

# Informações Adicionais:

- Inglês: Intermediário;
- Espanhol: Iniciante;
- Boa Comunicação;
- Agilidade;
- Trabalho em Equipe.
